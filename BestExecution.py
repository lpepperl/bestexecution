#gui stuff
import tkinter as tkinter
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import subprocess as sub
import webbrowser
import os.path
import shutil as shutil
from shutil import move
#datetime stuff
import datetime as datetime
from datetime import timedelta
#other
import pandas as pd
import random as r
import csv
import traceback
from openpyxl import load_workbook
import numpy as np

class Config():
    def __init__(self):
        #Expected date formats:
        self.ttdate = '%m/%d/%Y' #date format found in transactions table
        self.tbdate = '%Y-%m-%d %H:%M:%S' #date format found in trade blotter (currently useless)
        self.tbreceived = '%Y-%m-%d-%H.%M.%S.%f' #date format found in trade blotter (received)
        self.tbentry = '%Y-%m-%d-%H.%M.%S.%f' #date format ofund in trade blotter (entry)

        #Transactions Table
        self.ttdict = {}
        self.ttdict['accountnumber'] = 'Account Number'
        self.ttdict['activitytype'] = 'Activity Type'
        self.ttdict['symbol'] = 'Symbol'
        self.ttdict['cusip'] = 'CUSIP'
        self.ttdict['tradedate'] = 'Trade Date'
        self.ttdict['quantity'] = 'Quantity'
        self.ttdict['amount'] = 'Amount'
        self.ttdict['discretionary'] = 'Discretionary'
        self.ttdict['securitytype'] = 'Orgel Security Type - PA'

        #Best Execution Template
        self.betdict = {}
        self.betdict['accountnumber'] = 'Account Number'
        self.betdict['activitytype'] = 'Activity Type'
        self.betdict['symbol'] = 'Symbol'
        self.betdict['cusip'] = 'CUSIP'
        self.betdict['tradedate'] = 'Trade Date'
        self.betdict['quantity'] = 'Quantity'
        self.betdict['amount'] = 'Amount'
        self.betdict['price'] = 'Price'
        self.betdict['received'] = 'Order Received Time'
        self.betdict['entry'] = 'Order Entry Time'
        self.betdict['recap'] = 'Trade Recap Reference Number'

        #Trade Blotter
        self.tbdict = {}
        self.tbdict['accountnumber'] = 'ACCOUNT'
        self.tbdict['activitytype'] = 'BUY/SELL'
        self.tbdict['symbol'] = 'SYMBOL'
        self.tbdict['price'] = 'PRICE'
        self.tbdict['tradedate'] = 'TRADE-DATE'
        self.tbdict['quantity'] = 'QUANTITY'
        self.tbdict['amount'] = 'AMOUNT'
        self.tbdict['received'] = 'IBD ORDER RECEIVED DATE & TIME'
        self.tbdict['entry'] = 'ORDER ENTRY DATE & TIME'

class BT():
    def __init__(self, security, date):
        self.security = security
        self.date = date

class Gui():
    def __init__(self, root, frm, ttdict, betdict, tbdict, ttdate, tbdate, tbreceived, tbentry):
        self.ttdict = ttdict
        self.betdict = betdict
        self.tbdict = tbdict
        self.root = root
        self.frm = frm
        self.ttdate = ttdate
        self.tbdate = tbdate
        self.tbreceived = tbreceived
        self.tbentry = tbentry
        self.root.resizable(width=False, height=False)
        self.root.wm_title("Best Execution")
        self.frm.grid()
        self.var = tkinter.IntVar()
        self.defaultbg = self.frm.cget('bg')

        rows = 0
        while rows < 20:
            self.frm.rowconfigure(rows, weight=1)
            self.frm.columnconfigure(rows,weight=1)
            rows += 1

        #Title
        self.title = tkinter.Label(self.frm, text='Best Execution', font=("Calibri",20))
        self.title.grid(row=0,column=0, columnspan=4, sticky = 'WE')

        self.btlist = []

        #STEP 1
        self.stepOne = tkinter.LabelFrame(self.frm, text=" 1. Enter File Details: ")
        self.stepOne.grid(row=0, columnspan=8, sticky='WE', \
                     padx=5, pady=5, ipadx=5, ipady=5)

        self.inFileLbl_1 = tkinter.Label(self.stepOne, text="Select the Transactions Table File:")
        self.inFileLbl_1.grid(row=0, column=0, sticky='W', padx=5, pady=2)
        self.inFileTxt_1 = tkinter.Entry(self.stepOne)
        self.inFileTxt_1.grid(row=0, column=1, columnspan=13, sticky="WE", pady=3)
        self.inFileBtn_1 = tkinter.Button(self.stepOne, text="Browse ...", command = self.browseTT)
        self.inFileBtn_1.grid(row=0, column=14, sticky='WE', padx=5, pady=2)
        self.addBtn_1 = tkinter.Button(self.stepOne, text="Add File", command = self.checkTTFile)
        self.addBtn_1.grid(row=0, column= 15, sticky='WE', padx=5, pady=2)

        self.inFileLbl_2 = tkinter.Label(self.stepOne, text="Select the Best Execution Template File:")
        self.inFileLbl_2.grid(row=1, column=0, sticky='W', padx=5, pady=2)
        self.inFileTxt_2 = tkinter.Entry(self.stepOne)
        self.inFileTxt_2.grid(row=1, column=1, columnspan=13, sticky="WE", pady=3)
        self.inFileBtn_2 = tkinter.Button(self.stepOne, text="Browse ...", command = self.browseBET)
        self.inFileBtn_2.grid(row=1, column=14, sticky='WE', padx=5, pady=2)
        self.addBtn_2 = tkinter.Button(self.stepOne, text="Add File", command = self.checkBETFile)
        self.addBtn_2.grid(row=1, column=15, sticky='WE', padx=5, pady=2)

        self.inFileLbl_3 = tkinter.Label(self.stepOne, text="Select the Trade Blotter File:")
        self.inFileLbl_3.grid(row=2, column=0, sticky='W', padx=5, pady=2)
        self.inFileTxt_3 = tkinter.Entry(self.stepOne)
        self.inFileTxt_3.grid(row=2, column=1, columnspan=13, sticky="WE", pady=3)
        self.inFileBtn_3 = tkinter.Button(self.stepOne, text="Browse ...", command = self.browseTB)
        self.inFileBtn_3.grid(row=2, column = 14, sticky='WE', padx=5, pady=2)
        self.addBtn_3 = tkinter.Button(self.stepOne, text="Add File", command = self.checkTBFile)
        self.addBtn_3.grid(row=2, column = 15, sticky='WE', padx=5, pady=2)

        #STEP 2
        self.stepTwo = tkinter.LabelFrame(self.frm, text=" 2. Enter Block Trades: ")
        self.stepTwo.grid(row=1, columnspan=8, sticky='WE', \
                     padx=5, pady=5, ipadx=5, ipady=5)

        self.inBTLbl_1 = tkinter.Label(self.stepTwo, text="Enter Security:")
        self.inBTLbl_1.grid(row=0, column=0, sticky='WE', padx=5, pady=2)
        self.inBTTxt_1 = tkinter.Entry(self.stepTwo)
        self.inBTTxt_1.grid(row=0, column=3, columnspan=3, sticky="WE", pady=3)

        self.inBTLbl_2 = tkinter.Label(self.stepTwo, text="Enter Date:")
        self.inBTLbl_2.grid(row=0, column=6, sticky='WE', padx=5, pady=2)
        self.inBTTxt_2 = tkinter.Entry(self.stepTwo)
        self.inBTTxt_2.grid(row=0, column=8, columnspan=4, sticky="WE", pady=3)

        self.inBTBtn_1 = tkinter.Button(self.stepTwo, text="Add Block Trade", command = self.bTrades)
        self.inBTBtn_1.grid(row=0, column=12, sticky='WE', padx=5, pady=2)

        self.doneBTBtn = tkinter.Button(self.stepTwo, text="Done", command = self.done)
        self.doneBTBtn.grid(row=0, column=13, sticky='WE', padx=5, pady=2)

        #STEP 3
        self.stepThree = tkinter.LabelFrame(self.frm, text=" 3. Check Data: ")
        self.stepThree.grid(row=2, column = 0, columnspan=2, sticky='WE', \
                       padx=5, pady=5, ipadx=5, ipady=5)

        self.checkBtn_3 = tkinter.Button(self.stepThree, text="Check Data", command = self.checkData)
        self.checkBtn_3.grid(row=0, columnspan = 2, sticky='WE', padx=5, pady=2)

        #STEP 4
        self.stepFour = tkinter.LabelFrame(self.frm, text=" 4. Run Best Execution: ")
        self.stepFour.grid(row=2, column = 2, columnspan=2, sticky='WE', \
                      padx=5, pady=5, ipadx=5, ipady=5)

        self.saveBtn = tkinter.Button(self.stepFour, text = "Choose Save Loc.", command = self.save)
        self.saveBtn.grid(row = 0, column = 0, columnspan = 2, sticky = 'WE', padx=5, pady=2)
        self.saveBtn.config(state = 'disabled')

        self.runBtn_3 = tkinter.Button(self.stepFour, text="Run", command = self.run)
        self.runBtn_3.grid(row=0, column = 2, columnspan = 2, sticky='WE', padx=5, pady=2)
        self.runBtn_3.config(state = 'disabled')

        #Clear Data
        self.clearDatafrm = tkinter.LabelFrame(self.frm, text = "Clear All Entered Data")
        self.clearDatafrm.grid(row = 2, column = 4, columnspan = 2, sticky = 'WE', \
                          padx = 5, pady = 5, ipadx = 5, ipady = 5)

        self.clearData_button = Button(self.clearDatafrm, text = 'Clear', command = self.clearData)
        self.clearData_button.grid(row = 0, columnspan = 2, sticky = 'WE', padx=5, pady=2)

        #Help
        self.helpLf = tkinter.LabelFrame(self.frm, text="Testing Instructions")
        self.helpLf.grid(row=2, column=6, columnspan=2, sticky = 'WE', \
                    padx=5, pady=5, ipadx=5, ipady=5)

        self.helpButton = Button(self.helpLf, text="Help", command = self.gotoSite)
        self.helpButton.grid(row=0, columnspan = 2, sticky = 'WE', padx = 5, pady = 2)

        self.contactButton = Button(self.helpLf, text = "Contact Info", command = self.contactInfo)
        self.contactButton.grid(row = 0, column = 2, columnspan = 2, sticky = 'WE', padx = 5, pady = 2)

        #Message Center
        self.msg = tkinter.LabelFrame(self.frm, text="Message Center")
        self.msg.grid(row = 4, columnspan = 8, sticky = 'WE', \
                 padx=5, pady=5, ipadx=5, ipady=5)

        self.messagecenter = tkinter.Text(self.msg, state = 'disabled')
        self.messagecenter.grid(row = 0, columnspan = 8, sticky = 'WE', \
                           padx=5, pady=0, ipadx=5, ipady=0)
        self.messagecenter.tag_config('warning', background="yellow", foreground="red")
        self.messagecenter.tag_config('completed', background="green2")

        for i in range(0, 2):
            self.stepOne.columnconfigure(i, weight=1, uniform = 'foo')
    def filter(self, file):
        try:
            cfilter = 0
            fsplit = file.name.strip()[:-4]
            self.fwrite = fsplit + '_f' + '.csv'
            with open(file.name, 'r') as inp, open(self.fwrite, 'w', newline = '') as out:
                writer = csv.writer(out, quotechar = '"')
                for row in csv.reader(inp, quotechar = '"'):
                    if cfilter == 0:
                        writer.writerow(row)
                        cfilter += 1
                    if 'Exchange Traded' in row[-1] or 'Common Stock' in row[-1] or 'Preferred Stock' in row[-1]:
                        writer.writerow(row)
            shutil.move(self.fwrite, file.name)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Transactions Table filtered!\n')
            self.messagecenter.config(state = 'disabled')
            self.sortCombine(file)
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Failed filtering Transactions Table, check command prompt for details\n', 'warning')
            self.messagecenter.config(state = 'disabled')
    def sortCombine(self, file):
        c1 = self.ttdict.get('accountnumber')
        c2 = self.ttdict.get('activitytype')
        c3 = self.ttdict.get('symbol')
        c4 = self.ttdict.get('cusip')
        c5 = self.ttdict.get('tradedate')
        c6 = self.ttdict.get('quantity')
        c7 = self.ttdict.get('amount')
        c8 = self.ttdict.get('discretionary')
        c9 = self.ttdict.get('securitytype')
        try:
            #sort
            with open(file.name, newline = '') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ',')
                sortedlist = sorted(reader, key = lambda row:(row[c1], row[c3], row[c5], row[c2]), reverse = False)
            with open(file.name, 'w') as f:
                fieldnames = [c1, c2, c3, c4, c5, c6, c7, c8, c9]
                writer = csv.DictWriter(f, fieldnames = fieldnames)
                writer.writeheader()
                for row in sortedlist:
                    writer.writerow(row)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Transactions Table sorted!\n')
            self.messagecenter.config(state = 'disabled')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Failed sorting Transactions Table, check command prompt for details\n', 'warning')
            self.messagecenter.config(state = 'disabled')
            return 1
        try:
            #combine matching
            df = pd.read_csv(file.name, header = 0)
            df[c6] = df.groupby([c1, c3, c5, c2])[c6].transform('sum')
            df[c7] = df.groupby([c1, c3, c5, c2])[c7].transform('sum')
            df = df.drop_duplicates(keep = False)
            df.to_csv(file.name, index = False)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Transactions Table combined!\n')
            self.messagecenter.config(state = 'disabled')
        except Excetion as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Failed combining Transactions Table, check command prompt for details\n', 'warning')
            self.messagecenter.config(state = 'disabled')
    def done(self):
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Done inserting Block Trades\n')
        self.messagecenter.config(state = 'disabled')
        self.inBTTxt_1.config(state = 'disabled')
        self.inBTTxt_2.config(state = 'disabled')
        self.inBTBtn_1.config(state = 'disabled')
        self.doneBTBtn.config(state = 'disabled')
    def browseTT(self):
        file = filedialog.askopenfile(parent=self.root,mode='rb',title='Choose Transactions Table file')
        self.inFileTxt_1.delete(0, 'end')
        self.inFileTxt_1.insert(0, file.name)
        self.filter(file)
    def browseBET(self):
        file = filedialog.askopenfile(parent=self.root,mode='rb',title='Choose Best Execution Template file')
        self.inFileTxt_2.delete(0, 'end')
        self.inFileTxt_2.insert(0, file.name)
    def browseTB(self):
        file = filedialog.askopenfile(parent=self.root,mode='rb',title='Choose Trade Blotter file')
        self.inFileTxt_3.delete(0, 'end')
        self.inFileTxt_3.insert(0, file.name)
    def gotoSite(self):
        try:
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Loading README for testing instructions...\n')
            self.messagecenter.config(state = 'disabled')
            webbrowser.open('https://bitbucket.org/lpepperl/bestexecution/src/master/README.md', new = 0, autoraise = True)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Successfully loaded README for testing instructions!\n')
            self.messagecenter.config(state = 'disabled')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Unable to load README, check command prompt for details\n', 'warning')
            self.messagecenter.config(state = 'disabled')
    def contactInfo(self):
        self.popup = tkinter.Toplevel()
        self.popup.resizable(width=False, height=False)
        self.popup.wm_title("Contact Info")
        self.popupLbl_1 = tkinter.Label(self.popup, text="Logan Pepperl")
        self.popupLbl_1.grid(row=0, column=0, sticky='W', padx=5, pady=2)
        self.popupLbl_2 = tkinter.Label(self.popup, text="(715) 828-2200")
        self.popupLbl_2.grid(row=1, column=0, sticky='W', padx=5, pady=2)
        self.popupLbl_3 = tkinter.Label(self.popup, text="logan.pepperl@coloradocollege.edu")
        self.popupLbl_3.grid(row=2, column=0, sticky='W', padx=5, pady=2)
    def clearData(self):
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Clearing all data...\n')
        self.messagecenter.config(state = 'disabled')
        try:
            del self.btlist[:]
            self.inFileTxt_1.config(state = 'normal')
            self.inFileTxt_1.delete(0, 'end')
            self.inFileBtn_1.config(state = 'normal')
            self.addBtn_1.config(state = 'normal')
            self.inFileTxt_2.config(state = 'normal')
            self.inFileTxt_2.delete(0, 'end')
            self.inFileBtn_2.config(state = 'normal')
            self.addBtn_2.config(state = 'normal')
            self.inFileTxt_3.config(state = 'normal')
            self.inFileTxt_3.delete(0, 'end')
            self.inFileBtn_3.config(state = 'normal')
            self.addBtn_3.config(state = 'normal')
            self.inBTTxt_1.config(state = 'normal')
            self.inBTTxt_1.delete(0, 'end')
            self.inBTTxt_2.config(state = 'normal')
            self.inBTTxt_2.delete(0, 'end')
            self.inBTBtn_1.config(state = 'normal')
            self.doneBTBtn.config(state = 'normal')
            self.runBtn_3.config(state = 'disabled')
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'All data cleared!\n')
            self.messagecenter.config(state = 'disabled')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Unable to clear data, check command prompt for details\n', 'warning')
            self.messagecenter.config(state = 'disabled')
    def checkTTFile(self):
        file_path = self.inFileTxt_1.get()
        if not os.path.isfile(file_path):
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Transactions Table File not found\n', 'warning')
            self.messagecenter.config(state = 'disabled')
            return 1
        else:
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Transactions Table file found! Checking formatting...\n')
            self.messagecenter.config(state = 'disabled')
        self.TransactionsTable = pd.read_csv(file_path)
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, "Found 'Transactions Table' sheet!\n")
        self.messagecenter.config(state = 'disabled')
        for i in range (0, len(self.TransactionsTable.columns)):
            self.TransactionsTable.columns.values[i] = (self.TransactionsTable.columns[i]).strip()
        for col in self.ttdict.values():
            if not col in self.TransactionsTable:
                self.messagecenter.config(state = 'normal')
                self.messagecenter.insert(INSERT, "Error: Missing column %s in 'Transactions Table' sheet\n" % col, 'warning')
                self.messagecenter.config(state = 'disabled')
                return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, "All columns found in 'Transactions Table' sheet!\n")
        self.messagecenter.config(state = 'disabled')
        self.inFileTxt_1.config(state = 'disabled')
        self.inFileBtn_1.config(state = 'disabled')
        self.addBtn_1.config(state = 'disabled')
    def checkBETFile(self):
        file_path = self.inFileTxt_2.get()
        if not os.path.isfile(file_path):
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Best Execution Template file not found\n', 'warning')
            self.messagecenter.config(state = 'disabled')
            return 1
        else:
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Best Execution Template file found! Checking formatting...\n')
            self.messagecenter.config(state = 'disabled')
        try:
            self.BestExecutionTemplate = pd.read_excel(file_path, sheet_name = 'Best Execution Template')
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "'Best Execution Template' sheet found!\n")
            self.messagecenter.config(state = 'disabled')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "Error: 'Best Execution Template' sheet not found, check command prompt for details\n", 'warning')
            self.messagecenter.config(state = 'disabled')
            return 1
        for i in range (0, len(self.BestExecutionTemplate.columns)):
            self.BestExecutionTemplate.columns.values[i] = (self.BestExecutionTemplate.columns[i]).strip()
        for col in self.betdict.values():
            if not col in self.BestExecutionTemplate:
                self.messagecenter.config(state = 'normal')
                self.messagecenter.insert(INSERT, "Error: Missing column %s in 'Best Execution Template' sheet\n" % col, 'warning')
                self.messagecenter.config(state = 'disabled')
                return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, "All columns found in 'Best Execution Template' sheet!\n")
        self.messagecenter.config(state = 'disabled')
        self.inFileTxt_2.config(state = 'disabled')
        self.inFileBtn_2.config(state = 'disabled')
        self.addBtn_2.config(state = 'disabled')
    def checkTBFile(self):
        file_path = self.inFileTxt_3.get()
        #easter egg
        if file_path == 'BET':
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'BETTTTTTTTTTTTTTTT\n', 'completed')
            self.messagecenter.config(state = 'disabled')
            while 1:
                self.frm.config(bg = 'salmon')
                self.stepOne.config(bg = 'lawn green')
                self.stepTwo.config(bg = 'lawn green')
                self.stepThree.config(bg = 'lawn green')
                self.stepFour.config(bg = 'lawn green')
                self.clearDatafrm.config(bg = 'lawn green')
                self.helpLf.config(bg = 'lawn green')
                self.msg.config(bg = 'lawn green')
                root.update()
                self.frm.config(bg = 'lawn green')
                self.stepOne.config(bg = 'salmon')
                self.stepTwo.config(bg = 'salmon')
                self.stepThree.config(bg = 'salmon')
                self.stepFour.config(bg = 'salmon')
                self.clearDatafrm.config(bg = 'salmon')
                self.helpLf.config(bg = 'salmon')
                self.msg.config(bg = 'salmon')
                root.update()
            return 1

        if not os.path.isfile(file_path):
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Trade Blotter File not found\n', 'warning')
            self.messagecenter.config(state = 'disabled')
            return 1
        else:
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Trade Blotter File found!\n')
            self.messagecenter.config(state = 'disabled')
        try:
            self.Blotter = pd.read_excel(file_path, sheet_name = 'Blotter')
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "'Blotter' sheet found\n")
            self.messagecenter.config(state = 'disabled')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "'Blotter' sheet not found, check command prompt for details\n", 'warning')
            self.messagecenter.config(state = 'disabled')
            return 1
        for i in range (0, len(self.Blotter.columns)):
            self.Blotter.columns.values[i] = (self.Blotter.columns[i]).strip()
        for col in self.tbdict.values():
            if not col in self.Blotter:
                self.messagecenter.config(state = 'normal')
                self.messagecenter.insert(INSERT, "Error: Missing column %s in 'Blotter' sheet\n" % col, 'warning')
                self.messagecenter.config(state = 'disabled')
                return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, "All columns found in 'Blotter' sheet!\n")
        self.messagecenter.config(state = 'disabled')
        self.inFileTxt_3.config(state = 'disabled')
        self.inFileBtn_3.config(state = 'disabled')
        self.addBtn_3.config(state = 'disabled')
    def bTrades(self):
        security = self.inBTTxt_1.get()
        date = self.inBTTxt_2.get()
        try:
            datecheck = datetime.datetime.strptime(date, '%Y-%m-%d')
            securityup = security.upper()
            bt = BT(security, date)
            self.btlist.append(bt)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "Block Trade '%s %s' added!\n" % (security, date))
            self.messagecenter.config(state = 'disabled')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "Error: Bad date format (requires 'YYYY-MM-DD') or other issue, check command prompt for details\n", 'warning')
            self.messagecenter.config(state = 'disabled')
    def checkData(self):
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checking all data...\n')
        self.messagecenter.config(state = 'disabled')
        if str(self.inFileBtn_1['state']) == 'disabled' and str(self.inFileBtn_2['state']) == 'disabled' and \
            str(self.inFileBtn_3['state']) == 'disabled' and str(self.inBTBtn_1['state']) == 'disabled':
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Bext Execution is ready to run! Please choose save location!\n')
            self.messagecenter.config(state = 'disabled')
            self.saveBtn.config(state = 'normal')
            self.checkBtn_3.config(state = 'disabled')
        else:
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Data is incomplete, please include all data.\n', 'warning')
            self.messagecenter.config(state = 'disabled')
    def save(self):
        try:
            self.folder = filedialog.askdirectory()
            if self.folder != '':
                self.saveBtn.config(state = 'disabled')
                self.messagecenter.config(state = 'normal')
                self.messagecenter.insert(INSERT, 'Folder %s added as save location for run!\n' % self.folder)
                self.messagecenter.config(state = 'disabled')
                self.runBtn_3.config(state = 'normal')
                self.runBtn_3.config(bg = 'green2')
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, 'Error: Unable to add folder, check command prompt for details\n', 'warning')
            self.messagecenter.config(state = 'disabled')
    def run(self):
        self.runBtn_3.config(state = 'disabled')
        self.clearData_button.config(state = 'disabled')
        self.helpButton.config(state = 'disabled')
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Starting Best Execution process...\n')
        self.messagecenter.config(state = 'disabled')
        self.contactButton.config(state = 'disabled')
        self.root.update()
        try:
            r = Runner(self.TransactionsTable, self.BestExecutionTemplate, self.Blotter, self.ttdict, self.betdict, self.tbdict, self.btlist, self.folder, self.ttdate, self.tbdate, self.tbreceived, self.tbentry)
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "Failed to create 'Runner' class object, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 1\n')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.copyPasteAccess()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "copyPasteAccess method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 2\n')
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.blockTrade()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "blockTrade method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 3\n')
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.formatBlotter()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "formatBlotter method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 4\n')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.blotter()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "blotter method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 5\n')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.createSheets()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "createSheets method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 6\n')
        self.messagecenter.insert(INSERT, "Now, go to the Bloomberg terminal, open up 'preBloomberg.xlsx' in the save location, let the data load in, then save as 'Bloomberg.xlsx' in the same folder. Press the run button to continue.\n", 'completed')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.frm.config(bg = 'yellow')
        self.runBtn_3.config(state = 'normal', command = self.resume)
        self.root.update()
        self.runBtn_3.wait_variable(self.var)
        self.runBtn_3.config(state = 'disabled')
        self.frm.config(bg = self.defaultbg)
        self.root.update()
        try:
            r.finish()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "finish method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 7\n')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.extendRange()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "extendRange method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, 'Checkpoint 8\n')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.root.update()
        try:
            r.buildFinal()
        except Exception as e:
            print(e)
            traceback.print_tb(e.__traceback__)
            self.messagecenter.config(state = 'normal')
            self.messagecenter.insert(INSERT, "buildFinal method failed, check command prompt for details\n", 'warning')
            self.messagecenter.see("end")
            self.messagecenter.config(state = 'disabled')
            self.frm.config(bg = 'red')
            return 1
        self.messagecenter.config(state = 'normal')
        self.messagecenter.insert(INSERT, "Best Execution has completed! Please check the save folder for the 'Final Report' file!\n", 'completed')
        self.messagecenter.see("end")
        self.messagecenter.config(state = 'disabled')
        self.helpButton.config(state = 'normal')
        self.contactButton.config(state = 'normal')
        self.frm.config(bg = 'green2')
    def resume(self):
        self.var.set(1)

class Runner():
    def __init__(self, TransactionsTable, BestExecutionTemplate, Blotter, ttdict, betdict, tbdict, btlist, folder, ttdate, tbdate, tbreceived, tbentry):
        self.TransactionsTable = TransactionsTable
        self.BestExecutionTemplate = BestExecutionTemplate
        self.Blotter = Blotter
        self.ttdict = ttdict
        self.betdict = betdict
        self.tbdict = tbdict
        self.btlist = btlist
        self.folder = folder
        self.ttdate = ttdate
        self.tbdate = tbdate
        self.tbreceived = tbreceived
        self.tbentry = tbentry
    def copyPasteAccess(self):
        c1 = self.ttdict.get('discretionary')
        c2 = self.ttdict.get('securitytype')
        droplist = []
        #iterate over all rows in transactions table, looking for non discretionary
        for i, row in self.TransactionsTable.iterrows():
            if 'no' in self.TransactionsTable.iloc[i][c1].strip().lower():
                droplist.append(i)
        #delete all the non discretionary
        self.TransactionsTable = self.TransactionsTable.drop(droplist)
        #delete the discretionary and PA column
        self.TransactionsTable = self.TransactionsTable.drop(columns = [c2])
        self.TransactionsTable = self.TransactionsTable.drop(columns = [c1])
        #reset row index in dataframe
        self.TransactionsTable = self.TransactionsTable.reset_index(drop = True)
    def blockTrade(self):
        c1 = self.ttdict.get('symbol')
        c2 = self.ttdict.get('tradedate')
        #remove white space before and after all values in columns
        self.TransactionsTable[c1] = self.TransactionsTable[c1].str.strip()
        self.TransactionsTable[c2] = self.TransactionsTable[c2].str.strip()
        deletelist = []
        #iterate over all rows in transactions table, removing all block trades
        if len(self.btlist) > 0:
            for i in range (1, self.TransactionsTable.last_valid_index() + 1):
                try:
                    for bt in self.btlist:
                        #check if matching symbol and date found
                        if (self.TransactionsTable.iloc[i][c1]).upper() == bt.security and self.TransactionsTable.iloc[i][c2] == bt.date:
                            deletelist.append(i)
                except:
                    continue
            #remove all block trade rows
            self.TransactionsTable = self.TransactionsTable.drop(deletelist)
        #reset row index of dataframe
        self.TransactionsTable = self.TransactionsTable.reset_index(drop = True)
        #create a random sample of 10% of remaining rows in transactions table
        samplelist = r.sample(range(self.TransactionsTable.last_valid_index()), int(self.TransactionsTable.last_valid_index() / 10))
        for i in samplelist:
            #add these rows to a new dataframe
            self.BestExecutionTemplate = self.BestExecutionTemplate.append(self.TransactionsTable.iloc[i])
        #reset row imdex of dataframe
        self.BestExecutionTemplate = self.BestExecutionTemplate.reset_index(drop = True)
    def formatBlotter(self):
        tbc1 = self.tbdict.get('accountnumber')
        tbc2 = self.tbdict.get('symbol')
        tbc3 = self.tbdict.get('received')
        tbc4 = self.tbdict.get('entry')
        tbc5 = self.tbdict.get('quantity')
        tbc6 = self.tbdict.get('tradedate')
        tbc7 = self.tbdict.get('price')
        #strip whitespace before and after all column headers in Blotter
        for i in range (0, len(self.Blotter.columns)):
            self.Blotter.columns.values[i] = (self.Blotter.columns[i]).strip()
        #strip whitespace before and after all values in columns
        self.Blotter[tbc1] = self.Blotter[tbc1].str.strip()
        self.Blotter[tbc2] = self.Blotter[tbc2].str.strip()
        self.Blotter[tbc3] = self.Blotter[tbc3].str.strip()
        self.Blotter[tbc4] = self.Blotter[tbc4].str.strip()
        self.Blotter[tbc5] = self.Blotter[tbc5].str.strip()
        #iterate over all rows in blotter, converting date formats
        for i in range (0, self.Blotter.last_valid_index() + 1):
            try:
                self.Blotter.loc[i, tbc6] = datetime.datetime.strptime(str(self.Blotter.at[i, tbc6]), self.tbdate).strftime(self.ttdate)
                self.Blotter.loc[i, tbc3] = datetime.datetime.strptime((self.Blotter.at[i, tbc3]), self.tbreceived).strftime('%Y-%m-%d %H.%M.%S.%f')
                self.Blotter.loc[i, tbc4] = datetime.datetime.strptime((self.Blotter.at[i, tbc4]), self.tbentry).strftime('%Y-%m-%d %H.%M.%S.%f')
            except:
                continue
        #format the Quantity in Blotter
        for i in range(0, self.Blotter.last_valid_index() + 1):
            self.Blotter.loc[i, tbc5] = str(self.Blotter.loc[i, tbc5]).replace('+', '')
            self.Blotter.loc[i, tbc5] = str(self.Blotter.loc[i, tbc5]).replace('-', '')
            self.Blotter.loc[i, tbc5] = str(self.Blotter.loc[i, tbc5]).replace(',', '')
    def blotter(self):
        tbc1 = self.tbdict.get('accountnumber')
        tbc2 = self.tbdict.get('symbol')
        tbc3 = self.tbdict.get('received')
        tbc4 = self.tbdict.get('entry')
        tbc5 = self.tbdict.get('quantity')
        tbc6 = self.tbdict.get('tradedate')
        tbc7 = self.tbdict.get('price')
        betc1 = self.betdict.get('accountnumber')
        betc2 = self.betdict.get('tradedate')
        betc3 = self.betdict.get('symbol')
        betc4 = self.betdict.get('quantity')
        betc5 = self.betdict.get('received')
        betc6 = self.betdict.get('entry')
        betc7 = self.betdict.get('price')
        betc8 = self.betdict.get('recap')
        #if match found on account number, trade date, symbol, and quantity, grab price and datetime from blotter
        for i in range (0, self.BestExecutionTemplate.last_valid_index() + 1):
            for j in range (0, self.Blotter.last_valid_index() + 1):
                if (str(self.BestExecutionTemplate.at[i, betc1])).upper() == (str(self.Blotter.at[j, tbc1])).upper() and (self.BestExecutionTemplate.at[i, betc2]) == (self.Blotter.at[j, tbc6]) and (str(self.BestExecutionTemplate.at[i, betc3])).upper() == (str(self.Blotter.at[j, tbc2])).upper() and self.BestExecutionTemplate.at[i, betc4] == int(round(float(self.Blotter.at[j, tbc5]))):
                    self.BestExecutionTemplate.loc[i, betc5] = self.Blotter.at[j, tbc3]
                    self.BestExecutionTemplate.loc[i, betc6] = self.Blotter.at[j, tbc4]
                    self.BestExecutionTemplate.loc[i, betc7] = self.Blotter.at[j, tbc7]
                    break
        #reset row index in dataframe
        self.BestExecutionTemplate = self.BestExecutionTemplate.reset_index(drop = True)
        #add trade recap reference numbers for easier analysis in the end
        for i in range(0, self.BestExecutionTemplate.last_valid_index() + 1):
            self.BestExecutionTemplate.loc[i, betc8] = i + 1
    def createSheets(self):
        c1 = self.betdict.get('accountnumber')
        c2 = self.betdict.get('tradedate')
        c3 = self.betdict.get('symbol')
        c4 = self.betdict.get('quantity')
        c5 = self.betdict.get('amount')
        c6 = self.betdict.get('received')
        c7 = self.betdict.get('entry')
        c8 = self.betdict.get('price')
        c9 = self.betdict.get('recap')
        c10 = self.betdict.get('activitytype')
        #the file the output is written to, will overwrite
        excel_writer = pd.ExcelWriter(self.folder + '/preBloomberg.xlsx')
        #for every row in sample population confirms, create a new sheet, and add the data from that line to the new sheet
        for i, row in self.BestExecutionTemplate.iterrows():
            df = pd.DataFrame(columns=['TRANSACTIONCODE', 'START', 'SYMBOL', 'SumOfQuantity', 'Activity', 'Order Received Time (from Blotter)', 'Order Entry Time (from Blotter)', 'Price', 'Trade Recap Reference Number', 'Bloomberg Security', 'Bloomberg Start Time', 'Bloomberg End Time', 'Trade Executed Time'])
            df.loc[0, 'TRANSACTIONCODE'] = str(self.BestExecutionTemplate.at[i, c1]).upper()
            df.loc[0, 'START'] = self.BestExecutionTemplate.at[i, c2]
            df.loc[0, 'SYMBOL'] = str(self.BestExecutionTemplate.at[i, c3]).upper()
            df.loc[0, 'SumOfQuantity'] = self.BestExecutionTemplate.at[i, c4]
            df.loc[0, 'Activity'] = self.BestExecutionTemplate.at[i, c10]
            df.loc[0, 'Order Received Time (from Blotter)'] = self.BestExecutionTemplate.at[i, c6]
            df.loc[0, 'Order Entry Time (from Blotter)'] = self.BestExecutionTemplate.at[i, c7]
            df.loc[0, 'Price'] = self.BestExecutionTemplate.at[i, c8]
            df.loc[0, 'Trade Recap Reference Number'] = self.BestExecutionTemplate.at[i, c9]
            df.loc[0, 'Bloomberg Security'] = (self.BestExecutionTemplate.at[i, c3]).upper() + ' us equity'
            #create a range to grab data in (+-4min), and give += 1 sec for error
            try:
                df.loc[0, 'Bloomberg Start Time'] = ((datetime.datetime.strptime(str(self.BestExecutionTemplate.at[i, c7]), '%Y-%m-%d %H.%M.%S.%f') - timedelta(minutes = 4)).strftime('%#m/%#d/%Y %H:%M:%S'))
                df.loc[0, 'Bloomberg End Time'] = ((datetime.datetime.strptime(str(self.BestExecutionTemplate.at[i, c7]), '%Y-%m-%d %H.%M.%S.%f') + timedelta(minutes = 4)).strftime('%#m/%#d/%Y %H:%M:%S'))
                df.loc[0, 'Trade Executed Time'] = datetime.datetime.strptime(str(self.BestExecutionTemplate.at[i, c7]), '%Y-%m-%d %H.%M.%S.%f').strftime('%Y-%m-%d %H:%M:%S')
                df.loc[1, 'Trade Executed Time'] = ((datetime.datetime.strptime(str(self.BestExecutionTemplate.at[i, c7]), '%Y-%m-%d %H.%M.%S.%f') - timedelta(seconds = 1)).strftime('%Y-%m-%d %H:%M:%S'))
                df.loc[2, 'Trade Executed Time'] = ((datetime.datetime.strptime(str(self.BestExecutionTemplate.at[i, c7]), '%Y-%m-%d %H.%M.%S.%f') + timedelta(seconds = 1)).strftime('%Y-%m-%d %H:%M:%S'))
            except:
                pass
            #these cells aren't in memory yet, so need to add blank ones to create them
            for j in range(1, 6):
                df.loc[j, 'TRANSACTIONCODE'] = ''
            #add the BDH equation for the Bloomberg excel add-in
            df.loc[6, 'TRANSACTIONCODE'] = '=BDH($J$2,"TRADE",$K$2,$L$2,"Dir=V","Dts=S","TZ=New York","Sort=A","IntrRw=True","CondCodes=S","QRM=S","ExchCode=H","BrkrCodes=H","RPSCodes=H","RPTParty=H","RPTContra=H","BICMICCodes=H","Type=S","Price=S","Size=S","TradeTime=H","Yield=H","ActionCodes=H","IndicatorCodes=H","UpfrontPrice=H","Spread=H","UseDPDF=Y")'
            #save the sheets to excel
            df.to_excel(excel_writer, ('S%s' % str(int(self.BestExecutionTemplate.at[i, c9]))), index = False)
        excel_writer.save()
    def finish(self):
        #create a bunch of blank sheets
        Acceptable = pd.DataFrame()
        Unacceptable = pd.DataFrame()
        Missing = pd.DataFrame()
        BadRange = pd.DataFrame()
        Other = pd.DataFrame()
        #workbook to read from
        Bloomberg = pd.ExcelFile(self.folder + '/Bloomberg.xlsx')
        #workbook to write to, will overwrite
        excel_writer = pd.ExcelWriter(self.folder + '/BestExCompleted.xlsx', engine = 'xlsxwriter')
        values = []
        #for every sheet, check for a match based on time, symbol, and quantity
        for i in range(0, len(Bloomberg.sheet_names)):
            found = False
            df = Bloomberg.parse(i)
            del values[:]
            if df.at[0, 'Activity'] == 'Buy':
                trade = True
            else:
                trade = False
            data = pd.isnull(df.loc[6, 'START'])
            if data:
                Missing = Missing.append(df.iloc[0])
                found = True
                continue
            for j in range(6, df.last_valid_index() + 1):
                if (str(df.at[j, 'TRANSACTIONCODE']).strip() == str(df.at[0, 'Trade Executed Time']).strip() or str(df.at[j, 'TRANSACTIONCODE']).strip() == str(df.at[1, 'Trade Executed Time']).strip() or str(df.at[j, 'TRANSACTIONCODE']).strip() == str(df.at[2, 'Trade Executed Time']).strip()) and df.at[j, 'SYMBOL'] == df.at[0, 'Price'] and df.at[j, 'SumOfQuantity'] == df.at[0, 'SumOfQuantity']:
                    for k in range((j-5), (j+6)):
                        if k != j:
                            try:
                                values.append(df.at[k, 'SYMBOL'])
                            except:
                                continue
                    try:
                        minprice = min(values)
                        maxprice = max(values)
                    except:
                        BadRange = BadRange.append(df.iloc[0])
                        found = True
                        break
                    #check if our price was fair compared to surrounding prices
                    if trade == True:
                        if df.at[j, 'SYMBOL'] > maxprice:
                            Unacceptable = Unacceptable.append(df.iloc[0])
                            found = True
                            break
                        else:
                            Acceptable = Acceptable.append(df.iloc[0])
                            found = True
                            break
                    else:
                        if df.at[j, 'SYMBOL'] < minprice:
                            Unacceptable = Unacceptable.append(df.iloc[0])
                            found = True
                            break
                        else:
                            Acceptable = Acceptable.append(df.iloc[0])
                            found = True
                            break
            if found == False:
                Other = Other.append(df.iloc[0])
            df.to_excel(excel_writer, ('S%s' % (i+1)), index = False)
        #write to the excel sheets
        Acceptable.to_excel(excel_writer, 'Acceptable', index = False)
        Unacceptable.to_excel(excel_writer, 'Unacceptable', index = False)
        Missing.to_excel(excel_writer, 'Missing', index = False)
        BadRange.to_excel(excel_writer, 'Bad Range', index = False)
        Other.to_excel(excel_writer, 'Other', index = False)
        excel_writer.save()
    def extendRange(self):
        xls = pd.ExcelFile(self.folder + '/BestExCompleted.xlsx')
        Acceptable = pd.DataFrame()#xls, sheet_name = 'Acceptable')
        Unacceptable = pd.DataFrame()#xls, sheet_name = 'Unacceptable')
        Other = pd.read_excel(xls, sheet_name = 'Other')
        Missing = pd.DataFrame()#xls, sheet_name = 'Missing')
        OtherNew = pd.DataFrame()
        BadRange = pd.DataFrame()#xls, sheet_name = 'Bad Range')
        excel_writer = pd.ExcelWriter(self.folder + '/BestExCompleted_e.xlsx', engine = 'xlsxwriter')
        values = []
        for i, row in Other.iterrows():
            b = False
            df = xls.parse('S%d' % Other.at[i, 'Trade Recap Reference Number'])
            for j in range(1, 61):
                df.loc[j, 'Trade Executed Time'] = datetime.datetime.strptime(str(df.at[0, 'Trade Executed Time']), '%Y-%m-%d %H:%M:%S') + timedelta(seconds = j)
            found = False
            del values[:]
            if df.at[0, 'Activity'] == 'BY':
                trade = True
            else:
                trade = False
            for j in range(6, df.last_valid_index() + 1):
                for x in range(0, 61):
                    if (str(df.at[j, 'TRANSACTIONCODE']).strip() == str(df.at[x, 'Trade Executed Time']).strip() and (df.at[j, 'SYMBOL'] == df.at[0, 'Price'] or
                        round(df.at[j, 'SYMBOL'], 2) == round(df.at[0, 'Price'], 2) or round(df.at[j, 'SYMBOL'], 3) == round(df.at[0, 'Price'], 3)) and
                        df.at[j, 'SumOfQuantity'] == df.at[0, 'SumOfQuantity']):
                        for k in range((j-5), (j+6)):
                            try:
                                if k != j:
                                    values.append(df.at[k, 'SYMBOL'])
                                    minprice = min(values)
                                    maxprice = max(values)
                            except:
                                continue
                        #if buy:
                        try:
                            if trade == True:
                                if df.at[j, 'SYMBOL'] > maxprice:
                                    #highlight red
                                    Unacceptable = Unacceptable.append(df.iloc[0])
                                    found = True
                                    b = True
                                    break
                                else:
                                    #highlight green
                                    Acceptable = Acceptable.append(df.iloc[0])
                                    found = True
                                    b = True
                                    break
                            #if sell:
                            else:
                                if df.at[j, 'SYMBOL'] < minprice:
                                    #highlight red
                                    Unacceptable = Unacceptable.append(df.iloc[0])
                                    found = True
                                    b = True
                                    break
                                else:
                                    #highlight green
                                    Acceptable = Acceptable.append(df.iloc[0])
                                    found = True
                                    b = True
                                    break
                        except:
                            BadRange = BadRange.append(df.iloc[0])
                            found = True
                            b = True
                            break

                    if b == True:
                        break
            if found == False:
                OtherNew = OtherNew.append(df.iloc[0])
                df.to_excel(excel_writer, ('S%d' %  Other.at[i, 'Trade Recap Reference Number']), index = False)
        Acceptable.to_excel(excel_writer, 'Acceptable', index = False)
        Unacceptable.to_excel(excel_writer, 'Unacceptable', index = False)
        Missing.to_excel(excel_writer, 'Missing', index = False)
        BadRange.to_excel(excel_writer, 'Bad Range', index = False)
        OtherNew.to_excel(excel_writer, 'Other', index = False)
        excel_writer.save()
    def buildFinal(self):
        xls = pd.ExcelFile(self.folder + '/BestExCompleted.xlsx')
        xls_e = pd.ExcelFile(self.folder + '/BestExCompleted_e.xlsx')
        Acceptable = pd.read_excel(xls, sheet_name = 'Acceptable')
        Unacceptable = pd.read_excel(xls, sheet_name = 'Unacceptable')
        Missing = pd.read_excel(xls, sheet_name = 'Missing')
        BadRange = pd.read_excel(xls, sheet_name = 'Bad Range')
        Acceptable_e = pd.read_excel(xls_e, sheet_name = 'Acceptable')
        Unacceptable_e = pd.read_excel(xls_e, sheet_name = 'Unacceptable')
        Missing_e = pd.read_excel(xls_e, sheet_name = 'Missing')
        BadRange_e = pd.read_excel(xls_e, sheet_name = 'Bad Range')
        Other_e = pd.read_excel(xls_e, sheet_name = 'Other')
        c = self.betdict.get('recap')
        Acceptable = Acceptable.append(Acceptable_e, ignore_index = True)
        Unacceptable = Unacceptable.append(Unacceptable_e, ignore_index = True)
        Missing = Missing.append(Missing_e, ignore_index = True)
        BadRange = BadRange.append(BadRange_e, ignore_index = True)
        wb = load_workbook(self.folder + '/BestExCompleted.xlsx')
        writer = pd.ExcelWriter(self.folder + '/FinalReport.xlsx', engine = 'openpyxl')
        Acceptable['Link'] = np.nan
        Unacceptable['Link'] = np.nan
        Missing['Link'] = np.nan
        BadRange['Link'] = np.nan
        Other_e['Link'] = np.nan
        for i, row in Acceptable.iterrows():
            Acceptable.loc[i, 'Link'] = '=HYPERLINK("#S%s!A1","S%s")' % (Acceptable.at[i, c], Acceptable.at[i, c])
        for i, row in Unacceptable.iterrows():
            Unacceptable.loc[i, 'Link'] = '=HYPERLINK("#S%s!A1","S%s")' % (Unacceptable.at[i, c], Unacceptable.at[i, c])
        for i, row in BadRange.iterrows():
            BadRange.loc[i, 'Link'] = '=HYPERLINK("#S%s!A1","S%s")' % (BadRange.at[i, c], Acceptable.at[i, c])
        for i, row in Other_e.iterrows():
            Other_e.loc[i, 'Link'] = '=HYPERLINK("#S%s!A1","S%s")' % (Other_e.at[i, c], Other_e.at[i, c])
        writer.book = wb
        writer.sheets = dict((ws.title, ws) for ws in wb.worksheets)
        wb.remove(wb['Acceptable'])
        wb.remove(wb['Unacceptable'])
        wb.remove(wb['Missing'])
        wb.remove(wb['Bad Range'])
        wb.remove(wb['Other'])
        Acceptable.to_excel(writer, 'Acceptable_f', index = False)
        Unacceptable.to_excel(writer, 'Unacceptable_f', index = False)
        Missing.to_excel(writer, 'Missing_f', index = False)
        BadRange.to_excel(writer, 'Bad Range_f', index = False)
        Other_e.to_excel(writer, 'Other_f', index = False)
        writer.save()

if __name__ == '__main__':
    #Config
    c = Config()
    #Gui
    root = Tk()
    frm = tkinter.Frame(master = root)
    g = Gui(root, frm, c.ttdict, c.betdict, c.tbdict, c.ttdate, c.tbdate, c.tbreceived, c.tbentry)
    root.mainloop()
