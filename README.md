#Best Execution

The new and improved process for ensuring best execution

---

## Getting Started
***first time setup only**

These instructions will get the process up and running on your local machine for testing.


###Installing Python
The python programming language is required to run the bestex process

1. Navigate to **https://www.python.org/downloads/**.
2. Select release version **Python 3.6.5**. While you may be fine with a different version, newly released versions may not have the same capabilities.
3. Scroll down the page until you find **Files**.
4. Select the **Windows x86-64 executable installer**.
5. Open the .exe when it is done downoading.
6. **Run**.
7. You should now have a **Python Setup** window open, with two options, **Install Now** and **Customize installation**.
8. Make sure the boxes **Install launcher for all users** and **Add Python to PATH** are selected.
9. Select **Customize installation**.
10. Make sure all boxes in **Optional Features** are selected.
11. Hit **Next**
12. Under **Advanced Options**, look for **Customize install location**
13. Under **Customize install location**, type **'C:\'**. This will install python locally on your computer for all users.
14. Hit **Install**
15. To check if the installation was successful, navigage to the **Start** menu, and open the **Command Prompt**.
16. In the **Command Prompt**, type **'python'**. 
17. If the installation was **successful** you should see something like this:

	```
		Python 3.6.5 (v3.6.5:f59c0932b4, Mar 28 2018, 17:00:18) [MSC v.1900 64 bit (AMD64)] on win32
		Type "help", "copyright", "credits" or "license" for more information.
		>>>
	```

18. If the installation was **unsuccessful**, you should see something like this:

	```
		'python' is not recognized as an internal or external command,
		operable program or batch file.
	```	
	
19. If the installation was **unsuccessful**, try these steps again, or look for additional help.
20. Close the **Command Prompt**.

### Installing Libraries

There are additional libraries required to run BestEx that were not included in the Python install. **Pip** must be installed to complete this step, which should have been included in the Python install if done correctly.

1. Navigate to F:/APPS/Compliance/BestExecution/bestexecution/
2. Open **imports.txt**
3. If **imports.txt** does not exist in this directory, get it [here](https://bitbucket.org/lpepperl/bestexecution/src/master/imports.txt)
4. In **imports.txt**, you should see multiple "pip install <libraryname>" lines
5. Open the **Command Prompt**
6. Navigate to **C:/Python64/Scripts**
7. Now, once you are inside the **Scripts** folder, enter the first "pip install <libraryname>" eg:

	```
		C:\Python64\Scripts>pip install pandas
	```	

8. Repeat for the other lines.
9. Close the **Command Prompt**

---

## Running Best Execution

Step-by-step guide to ensure a successful run


**1. Create the folder**

You will need to create a folder that BestEx can output to. It is recommended to save all relevant files (Transactions Table, Best Execution Template, and Trade Blotter) in this folder for organizational purposes.

   1. Navigate to F:/APPS/Compliance/BestExecution
   2. Create a new folder, named for the data being run. For example, if this is being run for Q2 of 2018, name the folder '2018 Q2'
   
**2. Get the Transactions Table**

   1. Open the [**Bulk Reports**](https://portal.tamaracinc.com/BulkReports.aspx) page in **Tamarac**.
   2. Locate the **Best Execution** report.
   3. **Run Report**. *This report is set to run for the **previous quarter**. If you want to run it for a different time period, you will need to edit the report.
   4. Open the [**Bulk Report Status**](https://portal.tamaracinc.com/BulkReportStatus.aspx) page.
   5. Wait for the report to finish.
   6. **Download Report**.
   7. Once it is finished downloading, open the .zip    
   
      **IMPORTANT** Follow these next steps exactly, failure to do so may result in corrupted data.
   
   8. Hit **Extract**.
   9. Below where it says **Copy to:**, hit the ***...*** button on the right to browse for a folder.
   10. Select the folder you want the .csv file saved to.
   11. Hit **OK**.
   12. Now, go to the folder where you extracted the .csv file. You should see it saved there. ***It is required for this file to be a .csv, if it is saved as an excel sheet (.xlsx), it won't work.**
   
**3. Get the Best Execution Template File**

The Best Execution Template file is basically just a list of column headers. Because of this, It can sometimes be easier to recreate the file, rather than search for a previous version.

Option 1: Find the file  

   1. Navigate to F:/APPS/Compliance/BestExecution/Support - Reference.
   2. Hopefully, you will see an excel file called 'Best Execution Template'.
   3. If so, you can copy this file to a desired location, or just reference this file later.
   4. If not, you will need to create your own. (Option 2)

Option 2: Recreate the file

   1. Open a blank excel workbook.
   2. In the first row, you will need to create some column headers. ***It is important that you enter the headers exactly (no extra whitespace) as follows:**  
      Column A: **Account Number**  
	  Column B: **Activity Type**  
	  Column C: **Symbol**  
	  Column D: **CUSIP**  
	  Column E: **Trade Date**  
	  Column F: **Quantity**  
	  Column G: **Amount**  
	  Column H: **Price**  
	  Column I: **Order Received Time**  
	  Column J: **Order Entry Time**  
	  Column K: **Trade Recap Reference Number**  
	  
   3. Make sure the sheet is named '**Best Execution Template**'. (the filename doesn't matter, just the sheetname)
   4. Save the template to a desired location. 
   
**4. Get the Trade Blotter File**

   1. Navigate to F:/APPS/Pershing/Trading/20XX
   2. Open up the most current Trade Blotter
   3. **Optional but HIGHLY recommended:** Find the column the contains descriptions like 'ASSTBACK', 'EQUITY', and 'MUTFUND'. (should be **Column AA**') Filter the data, and **uncheck the 'MUTFUND' box**.
   4. Copy all the data in the sheet, including column headers, and paste it into a new workbook.
   5. Name the sheet '**Blotter**' (the filename doesn't matter, just the sheetname)
   6. Save the blotter file to a desired location.

**5. Run BestEx Using the GUI**

Using the GUI can be a little duanting at first, so it is important to have a general understanding of how the buttons work.  

   Step 1. Enter File Details:  
   >**Browse..**: This button lets you browse for a file to add. Specifically for the 'Transactions Table' button, it also filters the data.  
   >**Add File**: This button checks to see if the file you selected with the 'Browse..' button is in a recognizable format, with all necessary data present.  
   
   Step 2. Enter Block Trades:  
   >**Add Block Trade**: Once you enter the info for  the security and date of a block trade, you can use this button to store the info in memory. This allows you to input as many block trades as needed, or none at all.  
   >**Done**: This button is used to tell the computer when you are done entering block trades. Simply click it when you have no more block trade data to enter.  
   
   Step 3. Check Data:  
   >**Check Data**: Once you are done entering all the data for the files and block trades, use this button to check and make sure you have entered everything correctly. If everything looks good, unlocks the 'Choose Save Loc.' button.  
   
   Step 4. Run Best Execution:  
   >**Choose Save Loc.**: This button lets you browse for a folder to save the output files in. Unlocks the 'Run' button.  
   >**Run**: Once all data is entered and checked, the run button is unlocked. This is what actually runs BestEx. It is also used to resume running after the 'Bloomberg' step.  
   
   Clear All Entered Data  
   >**Clear**: This button is used to clear all entered data. This can be used if you accidentally enter an incorrect block trade, or an older version of a file. Alternatively, you could just close the GUI and reopen it.  
   
   Testing Instructions  
   >**Help**: The help button is a link to this README.md file. Use this button if you are ever unsure of what to do.  
   >**Contact Info**: If the instructions don't make sense, and you can't find any help from inside the firm or online, or it looks like major changes might be needed to BestEx, I'm here to help.  
   
   Message Center  
   >**Message Center**: The message center is how the computer will talk to you regarding BestEx. Ff you do anything wrong, you will see a simplified error message here, or a more detailed one in the Command Prompt.  
   
**Time to Run:**

*This sequence of steps assumes that you do everything correctly. If you get an error message in the command prompt, you will have to repeat steps.

   1. Open the **Command Prompt**
   2. In the **Command Prompt**, navigate to F:/APPS/Compliance/BestExecution/bestExecution. (use 'cd <directory name>' or 'cd ..' to navigate/go back)
   3. In the **Command Prompt**, type '**BestExecution.py**'. This should open the BestEx GUI. If not, check the **Command Prompt** for error messages.
   4. Hit the first '**Browse..**' button, and select the '**Transactions Table**' file from **2. Get the Transactions Table**.
   5. **Add File**
   6. Hit the second '**Browse..**' button, and select the '**Best Execution Template**' from **3. Get the Best Execution Template File**.
   7. **Add File**
   8. Hit the third '**Browse..**' button, and select the '**Trade Blotter**' from **4. Get the Trade Blotter File**.
   9. **Add File**
   10. For every block trade, not including mutal funds, that occured during the testing quarter, add the security and symbol to their respective boxes. This requires a specific formatting: |SYMBL| |YYY-MM-DD| eg. |MLPX| |2018-01-27|
   11. Hit **Add Block Trade** after every pair entered.
   12. Hit **Done** when done inserting block trades.
   14. **Check Data**
   15. **Choose Save Loc.**. Select the folder from **1. Create the Folder**.
   16. **Run**
   17. Wait for BestEx to finish running. This will be signified by either the GUI turning yellow, meaning the first part of BestEx was successful, or the GUI turning red, meaning BestEx encountered a fatal error.
   18. If the first part was successsful, you should see an excel file called 'preBloomberg' in your folder.
   19. Now, go to the Bloomberg terminal, make sure the Bloomberg Interface is logged in, and open up the 'preBloomberg' file.
   20. Let all the data load in from Bloomberg, you should be able to see a lot of new data in each sheet when it is done.
   21. Save this updated file as '**Bloomberg**' in the same folder.
   22. Go back to the computer running BestEx, and hit **Run** to resume running.
   23. Wait for BestEx to finish running. This will be signified be either the GUI turning green, meaning BestEx was successful, or the GUI turning red, meaning BestEx encountered a fatal error.
   24. If BestEx was successful, you should see a '**BestExCompleted**', '**BestExCompleted_e**', and '**FinalReport**' file.
   
   **Congratulations on successfully running Best Execution!**

**6. Understanding the Output**

The final output of BestEx is comprised of three different files, 'BestExCompleted', 'BestExCompleted_e', and 'FinalReport'.  
>**BestExCompleted** is the output of the standard runthrough of BestEx.  
>**BestExCompleted_e** is the basically the same as the BestExCompleted file, but it ignores small time discrepancies. It is a reanalysis of the trades that weren't originally found in Bloomberg. It only displays the sheets of the trades that failed the analysis twice.  
>**FinalReport** is the combination of BestExCompleted and BestExCompleted_e, with some links for easier analysis. This is the data that matters.  

The FinalReport file contains many sheets. In order to understand the output, you need to understand what each sheet means.  
>**Sheets S1 - Sn**: These sheets show our individual trades, and surrounding trades from Bloomberg. These sheets can be useful to look at to understand why certain trades failed.  
>**Acceptable**: This sheet is a list of all the trades that were successfully found in Bloomberg, and determined to be acceptable compared to surrounding trades.  
>**Unacceptable**: This sheet is a list of all the trades that were successfully found in Bloomberg, but determined to be unacceptable compared to surrounding trades.  
>**Bad Range**: This sheet is a list of all the trades that were successfully found in Bloomberg, but didn't have any data to compare to. (this shouldn't really happen)  
>**Missing**: This sheet is a list of all the trades that we were missing data for.  
>**Other**: This sheet is a list off all the trades that were never matched in the Bloomberg data, usually due to a significant time error, or a split quantity.  

---

## Handling Crashes

---

## Editing Best Execution

###Installing Git (Optional)
The Git commands can be extremely usefull for updating, editing, and safely managing the code. If you need to make changes to BestEx, or are one of the primary users, it is highly recommended to follow these steps.

1. Click [here](https://git-scm.com/download/win) to begin download.
2. Open the .exe when it is done downloading.
3. **Run**.
4. more Git instructions to come...

###Git Usage
[Full Git Tutorial](https://git-scm.com/docs/gittutorial)

Useful Git commands:

1. **git config**
2. **git init**		
3. **git clone**
4. **git pull**
5. **git status**
6. **git diff**
7. **git add**
8. **git commit**
9. **git push**
10. **git checkout**

###Installing Atom (Optional)
Atom is a text editor that is great for viewing and editing the code for BestEx. If you need to make changes to BestEx, or are one of the primary users, it is highly recommended to follow these steps.

1. Navigate to **https://atom.io/**
2. Open the .exe when it is done downloading.
3. **Run**. Atom should do everything automatically.
4. To use Atom, navigate to where the file is saved, and select "Open with Atom"
5. If this option doesn't exist, you can just open Atom, and browse for the file from there.

---

## Additional Help
Understanding weird errors and edge cases.

